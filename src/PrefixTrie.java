import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class PrefixTrie implements TextPredictor{

	private Node root;
	public PrefixTrie() {
		root = new Node();
	}
	
	@Override
	public void insert(String s) {
		root.insert(s);
	}
	
	@Override
	public Collection<String> predict(String s){
		Node n = root.followPath(s);
		if(n == null) {
			return Collections.emptyList();
		}
		return n.values();
	}
	
	
	public static final class Node {
		private String value;
		private Node[] children;

		public Node() {
			this.children = new Node[26]; // 26 is the number of lower case letters between 'a' and 'z'
			this.value = null;
		}
		
		public void insert(String s) {
			Node current = this;
			for(char c : s.toCharArray()) {

				Node n = current.children[c - 'a'];
				if(n == null) {
					n = current.children[c - 'a'] = new Node();
				}
				current = n;
			}
			current.value = s;
		}
		
		public Node followPath(String s) {
			Node current = this;
			for(char c : s.toCharArray()) {
				Node n = current.children[c - 'a'];
				if(n == null) {
					return null;
				}
				current = n;
			}
			return current;
		}
		
		
		public Collection<String> values(){
			List<String> values = new ArrayList<>();
			if(this.value != null)
				values.add(this.value);
			
			for(int i = 0; i < this.children.length; i++) {
				Node n = this.children[i];
				if(n != null) {
					values.addAll(n.values());
				}
			}
			return values;
			
		}
		
	}

}
