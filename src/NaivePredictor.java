import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class NaivePredictor implements TextPredictor{

	
	public List<String> words = new ArrayList<String>();

	@Override
	public void insert(String word) {
		this.words.add(word);
	}
	
	
	@Override
	public Collection<String> predict(String s) {
		List<String> results = new ArrayList<String>();
		for(String w : words) {
			if(w.startsWith(s)) {
				results.add(w);
			}
		}
		return results;
	}

}
