import java.util.Collection;

public interface TextPredictor {
	void insert(String s);
	Collection<String> predict(String s);
}
