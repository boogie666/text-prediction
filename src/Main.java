import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.Scanner;
import java.util.Stack;

public class Main {

	public static void main(String[] args) throws FileNotFoundException {
		TextPredictor naivePredictor = naive("words.txt");
		TextPredictor triePredictor = trie("words.txt");
		
		bench(naivePredictor, "hello");
		bench(triePredictor, "hello");
		
		

	}

	public static void bench(TextPredictor p, String searchTerm) {
		double avgTime = 0;
		Collection<String> values = null;
		for (int i = 0; i < 100; i++) {
			double time = System.currentTimeMillis();

			values = p.predict(searchTerm);

			double endTime = System.currentTimeMillis();

			avgTime += (endTime - time);
		}

		System.out.println(
				String.format("%5s took %.3fms on avarage and found %d results", p.getClass().getSimpleName(), avgTime / 100, values.size()));

	}

	private static TextPredictor naive(String filePath) throws FileNotFoundException {
		TextPredictor naive = new NaivePredictor();
		loadWords(naive, filePath);
		return naive;
	}

	private static TextPredictor trie(String filePath) throws FileNotFoundException {
		TextPredictor p = new PrefixTrie();
		loadWords(p, filePath);
		return p;
	}

	private static void loadWords(TextPredictor p, String filePath) throws FileNotFoundException {
		Scanner s = new Scanner(new FileInputStream(new File(filePath)));
		while (s.hasNext()) {
			p.insert(s.nextLine());
		}
		s.close();
	}

}
